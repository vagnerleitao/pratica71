
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;
import utfpr.ct.dainf.if62c.pratica.JogadorComparator;
import utfpr.ct.dainf.if62c.pratica.Time;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * IF62C - Fundamentos de Programação 2
 * 
 * Template de projeto de programa Java usando Maven.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {
    
    private static List<Jogador> conjunto = new ArrayList<>();
    private static int numItens=-1;
    private static int numJog;
    private static String nomJog;
    
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Digite o número de jogadores a serem lidos : ");
        while (!scanner.hasNextInt()) {
            scanner.nextLine();
            System.out.println("Digite um valor numérico maior que 0 (zero)!");
            //scanner = new Scanner(System.in);
            System.out.print("Digite o número de jogadores a serem lidos : ");
        }
        numItens = scanner.nextInt();
        while(numItens<=0){
            //scanner.nextLine();
            System.out.println("Número de Jogadores Inválido!!");
            System.out.println("Digite um valor numérico maior que 0 (zero)!");
            System.out.print("Digite o número de jogadores a serem lidos : ");
            //scanner = new Scanner(System.in);
            if(!scanner.hasNextInt()) {
                scanner.nextLine();
                //System.out.print("Digite um valor numérico!\n");
                //System.out.println("Digite um valor numérico maior que 0 (zero)!\n");
                //scanner = new Scanner(System.in);
                //System.out.println("Digite o número de jogadores a serem lidos : ");
            }
            else{
               numItens = scanner.nextInt();
            }
        }
        
        System.out.println("Numero de jogadores a serem lidos: "+numItens);

        for(int i=0;i<numItens;i++){
            System.out.print("Digite o número do jogador : ");
            while (!scanner.hasNextInt()) {
                scanner.nextLine();
                System.out.println("Digite um valor numérico maior que 0(zero)!");
                //scanner = new Scanner(System.in);
                System.out.print("Digite o número do jogador : ");
            }
            numJog = scanner.nextInt();
            while(numJog<=0){
                //scanner.nextLine();
                System.out.println("Número de Jogadores Inválido!!");
                System.out.println("Digite um valor numérico maior que 0 (zero)!");
                System.out.print("Digite o número do jogador : ");
                //scanner = new Scanner(System.in);
                if(!scanner.hasNextInt()) {
                    scanner.nextLine();
                    //System.out.println("Digite um valor numérico!\n");
                    //scanner = new Scanner(System.in);
                    //System.out.println("Digite o número de jogadores a serem lidos : ");
                }
                else{
                    numJog = scanner.nextInt();
                }
            }            
            System.out.print("Digite o nome do jogador : ");
            nomJog = scanner.next();
            conjunto.add(new Jogador(numJog,nomJog));
        }
       
        Collections.sort(conjunto);
        conjunto.stream().forEach((joga) -> {
            System.out.println(joga);
        });
        
        numJog = -1;
        int achou = 0;
        while(numJog != 0){
            System.out.print("Digite um novo número de jogador ou 0 (zero) para sair : ");
            while (!scanner.hasNextInt()) {
                scanner.nextLine();
                System.out.println("Digite um valor numérico!");
                //scanner = new Scanner(System.in);
                System.out.print("Digite um novo número de jogador ou 0 (zero) para sair : ");
            }
            numJog = scanner.nextInt();
            while(numJog<0){
                //scanner.nextLine();
                System.out.println("Número de Jogador Inválido!!");
                System.out.print("Digite um novo número de jogador ou 0 (zero) para sair : ");
                //scanner = new Scanner(System.in);
                if(!scanner.hasNextInt()) {
                    scanner.nextLine();
                    //System.out.println("Digite um valor numérico!");
                    //scanner = new Scanner(System.in);
                    //System.out.println("Digite o número de jogadores a serem lidos : ");
                }
                else{
                    numJog = scanner.nextInt();
                }
            }            
            if(numJog > 0){
                achou = Collections.binarySearch(conjunto, new Jogador(numJog,null));
                System.out.print("Digite o nome do jogador : ");
                nomJog = scanner.next();
                if(achou<0) conjunto.add(new Jogador(numJog,nomJog));
                else conjunto.set(achou, new Jogador(numJog,nomJog));
                Collections.sort(conjunto);
                conjunto.stream().forEach((joga) -> {
                    System.out.println(joga);
                });
            }
        }
        
        Collections.sort(conjunto);
        for(Jogador joga : conjunto) System.out.println(joga);
        
        //numItens = conjunto.size();
        
        //scanner = new Scanner(System.in);
        //Double teste = scanner.nextDouble();
/*        Time valor1 = new Time();
        JogadorComparator valores = new JogadorComparator(false,true,false);
        List<Jogador> valor = valor1.ordena(valores);
        for(Jogador joga : valor) System.out.println(joga);
        
        int valor_joga =12;
        int achou = Collections.binarySearch(valor, new Jogador(valor_joga,null));
        if(achou>=0) System.out.println("O jogador de numero "+valor.get(achou).getNumero()+" é : "+valor.get(achou).getNome());
        else  System.out.println("O jogador de numero "+valor_joga+" nao esta na lista\n");
*/        

    }
    

}
