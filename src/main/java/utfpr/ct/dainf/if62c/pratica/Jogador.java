/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author vagner
 */
public class Jogador implements Comparable<Jogador> {
    private int numero;
    private String nome;

    
    
    
    public Jogador(int numero, String nome) {
        this.numero = numero;
        this.nome = nome;
    }

    public void setNome(String valor_nome){
        this.nome=valor_nome;
    }
    
    public void setNumero(int valor_numero){
        this.numero=valor_numero;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public int getNumero(){
        return this.numero;
    }
    
    
    @Override
    public String toString() {
        return numero + " - " + nome;
    }

    @Override
    public int compareTo(Jogador jogador) {
        return numero - jogador.numero;
    }
}
