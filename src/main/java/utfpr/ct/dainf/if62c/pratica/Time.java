/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author vagner
 */
public class Time {

    private static List<Jogador> conjunto = new ArrayList<>();
    private HashMap<String, Jogador> jogadores = new HashMap<>();
    private int posicao;
    private String nome;
    private boolean numero=false;
    private boolean num_asc=false;
    private boolean nome_asc=false;
    
    public Time(int posicao, String nome){
        this.posicao=posicao;
        this.nome=nome;
        //System.out.printf("Posicao = %d Nome = %s\n",this.posicao,this.nome);
    }

    public Time() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    

    public HashMap<String, Jogador> getJogadores(){
        return this.jogadores;
    }

    public void addJogador(String posicao, Jogador dados){
        this.jogadores.put(posicao, dados);
    }
    
    public List<Jogador> ordena(JogadorComparator valor){
        JogadorComparator retorno = valor;
        this.numero = retorno.numero;       // verdadeiro -> ordenação por número
        this.num_asc = retorno.num_asc;     // verdadeiro -> ordenação por número ascendente
        this.nome_asc = retorno.nome_asc;   // verdadeiro -> ordenação por nome ascendente
        int fator;
        conjunto.add(new Jogador(12,"Felix"));
        conjunto.add(new Jogador(10,"Roberto Rivelino"));
        conjunto.add(new Jogador(6,"Toninho"));
        conjunto.add(new Jogador(10,"Ronaldo"));
        conjunto.add(new Jogador(12,"Aranha"));
        conjunto.add(new Jogador(2,"Durval"));
        conjunto.add(new Jogador(2,"Carlos Alberto"));
        conjunto.add(new Jogador(6,"Pirlo"));
        conjunto.add(new Jogador(8,"Ronaldo"));
        //Collections.sort(conjunto,Collections.reverseOrder());
        if(numero){
            if(num_asc) Collections.sort(conjunto);
            else Collections.sort(conjunto,Collections.reverseOrder());
            if(nome_asc){
                // Busca elementos com números iguais e ordena em ordem crescente de nome
                int tamanho = conjunto.size();
                for(int i=0;i< tamanho;i++){
                    for(int k=i+1;k<tamanho;k++){
                        if(conjunto.get(i).getNumero()==conjunto.get(k).getNumero()){
                            String valor0 = conjunto.get(i).getNome();
                            String valor1 = conjunto.get(k).getNome();
                            Jogador joga0 = conjunto.get(i);
                            Jogador joga1 = conjunto.get(k);
                            if(valor0.compareTo(valor1)>0){
                                conjunto.set(i, joga1);
                                conjunto.set(k, joga0);
                                //System.out.println("valor0 = "+valor0+"valor1 = "+valor1);
                            }
                        }
                    }
                }
            }
            else{
                // Busca elementos com números iguais e ordena em ordem decrescente de nome
                int tamanho = conjunto.size();
                for(int i=0;i< tamanho;i++){
                    for(int k=i+1;k<tamanho;k++){
                        if(conjunto.get(i).getNumero()==conjunto.get(k).getNumero()){
                            String valor0 = conjunto.get(i).getNome();
                            String valor1 = conjunto.get(k).getNome();
                            Jogador joga0 = conjunto.get(i);
                            Jogador joga1 = conjunto.get(k);
                            if(valor0.compareTo(valor1)<0){
                                conjunto.set(i, joga1);
                                conjunto.set(k, joga0);
                                //System.out.println("valor0 = "+valor0+"valor1 = "+valor1);
                            }
                        }
                    }
                }                
            }
        //    Comparator<Jogador> porNumero = (e1, e2) -> Integer.compare(
        //    e1.getNumero(), e2.getNumero());
        }
        else {
            Collections.sort(conjunto, new Comparator<Jogador>() {
                @Override
                public int compare(Jogador joga1, Jogador joga2) {
                    int fator;
                    if(nome_asc) fator=1; else fator=-1;
                    return fator*joga1.getNome().compareTo(joga2.getNome());
                }
            });
            if(num_asc){
                int tamanho = conjunto.size();
                for(int i=0;i< tamanho;i++){
                    for(int k=i+1;k<tamanho;k++){
                        String valor0 = conjunto.get(i).getNome();
                        String valor1 = conjunto.get(k).getNome();
                        if(valor0.compareTo(valor1)==0){
                            Jogador joga0 = conjunto.get(i);
                            Jogador joga1 = conjunto.get(k);
                            if(conjunto.get(i).getNumero()>conjunto.get(k).getNumero()){
                                conjunto.set(i, joga1);
                                conjunto.set(k, joga0);
                            }
                        }

                    }
                }
            }
        }
        return conjunto;
    }
    
    
}
