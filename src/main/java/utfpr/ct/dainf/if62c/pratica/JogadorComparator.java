/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author vagner
 */
public class JogadorComparator implements Comparator<Jogador> {

    boolean numero=false;
    boolean num_asc=false;
    boolean nome_asc=false;
    
    public JogadorComparator(){ // Ordena por número e nome em ordem ascendente
        this.numero=true;   // false = nome
        this.nome_asc=true; // false = nome_desc

        Time valor1 = new Time();
        List<Jogador> valor = valor1.ordena(new JogadorComparator(true,true,true));
        for(Jogador joga : valor) System.out.println(joga);
        

        //Comparator<Jogador> porNumero = (e1, e2) -> Integer.compare(
        //    e1.getNumero(), e2.getNumero());

        //Time.stream().sorted(porNumero)
        //    .forEach(e -> System.out.println(e));
        
        
    }
    
    
    public JogadorComparator(boolean numero, boolean num_asc, boolean nome_asc){
        this.numero = numero;
        this.num_asc = num_asc;
        this.nome_asc = nome_asc;
        //if(numero) Collections.sort(conjunto, numero);
        
        
    }
    
//Dentro do pacote utfpr.ct.dainf.if62c.pratica,
//crie um comparador para a classe Jogador, isto é,
//crie a classe JogadorComparator que implementa a interface Comparator<Jogador>.
//Esta classe deverá ter a opção de comparar em ordem ascendente ou descendente
//de número e/ou nome, isto é, quando comparando por número, se forem iguais,
//comparar pelo nome, quando comparando por nome, se forem iguais, comparar pelo número.
//Esta classe deve implementar um construtor que receba três valores boolean:
//o primiero, se verdadeiro, indica ordenação por número,
//o segundo, se verdadeiro, indica que desejamos ordenar por número ascendente
//e o terceiro argumento, se verdadeiro, indica que desejamos ordenar por nome ascendente.
//O construtor padrão deve ser equivalente a ordenar por número e nome em ordem ascendente.
    
    
    //public List<Jogador> JogadorComparator (JogadorComparator valor){
    //    List<Jogador> dados = Arrays.asList(new Jogador(12,"Felix"));
        
    //    return dados;
    //}
    
    
    
    @Override
    public int compare(Jogador joga1, Jogador joga2) {
        int comparacao = joga1.compareTo(joga2);
        return comparacao;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
/*        int comparacao=0;
        if(numero) {
            if(joga1.numero == joga2.numero){
                comparacao = joga1.nome.compareTo(joga1.nome);
            }
            else{
                comparacao = joga1.compareTo(joga2);
            }
        }
        else{
            if(nome_asc){
                if(joga1.nome.equals(joga1.nome)){
                    comparacao = joga1.compareTo(joga2);
                }
                else{
                    comparacao = joga1.nome.compareTo(joga1.nome);
                }
            }
        }
        
        
        //int comparacao = joga1.compareTo(joga2);
        if(num_asc) return comparacao; else return -comparacao;
        //if(joga1.compareTo(joga2) == true) return comparacao;
        */
        }
    
}
